
<p align="center">
  <img src="img/fastAPI.png" alt="FastAPI">
</p>

## Welcome to my FastAPI repo
------

> **Documentation**: <a href="https://fastapi.tiangolo.com" target="_blank">
https://fastapi.tiangolo.com</a>

> **Source Code**: <a href="https://github.com/tiangolo/fastapi" 
target="_blank">https://github.com/tiangolo/fastapi</a>

-------

This repo is based on Michael Herman's blog at [testdriven.io](https://testdriven.io/blog/fastapi-crud/), but I put a detailed tutorial produced by MKdocs inside this repo to make it more user friendly.

Please access this tutorial [GitLab Page](https://rhkina.gitlab.io/fastapi/) while you follow this repo!

FastAPI is a modern, fast (high-performance), web framework for building APIs with Python 3.6+ based on standard Python type hints.

Historically, async work in Python has been nontrivial (though its API has rapidly improved since Python 3.4) particularly with Flask. Essentially, Flask (on most WSGI servers) is blocking by default - work triggered by a request to a particular endpoint will hold the server entirely until that request is completed. Instead, Flask (or rather, the WSGI server running it, like `gunicorn` or `uWSGI`) achieve scaling by running multiple worker instances of the app in parallel, such that requests can be farmed to other workers while one is busy. Within a single worker, asynchronous work can be wrapped in a blocking call (the route function itself is still blocking), threaded (in newer versions of Flask), or farmed to a queue manager like `Celery` - but there isn’t a single consistent story where routes can cleanly handle asynchronous requests without additional tooling.

FastAPI is designed from the ground up to run asynchronously - thanks to its underlying `starlette` ASGI framework, route functions default to running within an asynchronous event loop. With a good ASGI server (FastAPI is designed to couple to `uvicorn`, running on top of `uvloop`) this can get us performance on par with fast asynchronous webservers in Go or Node, without losing the benefits of Python’s broader machine learning ecosystem.

In contrast to messing with threads or Celery queues to achieve asynchronous execution in Flask, running an endpoint asynchronously is dead simple in FastAPI - we simply declare the route function as asynchronous (with `async def`) and we’re ready to go! We can even do this if the route function isn’t conventionally asynchronous - that is, we don’t have any awaitable calls (like if the endpoint is running inference against an ML model). In fact, unless the endpoint is specifically performing a blocking IO operation (to a database, for example), it’s better to declare the function with `async def` (as blocking functions are actually punted to an external threadpool and then awaited anyhow).
